from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string

from .views import home_page


# Create your tests here.
class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)
    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('lists/home.html')
        # decode(): bits to string
        self.assertEqual(response.content.decode(), expected_html)

        # Do not test constants.
        # self.assertTrue(response.content.startswith(b'<!DOCTYPE html>'), repr(response.content))

        self.assertIn(b'<title>To-Do lists</title>', response.content)
        print()
        self.assertTrue(response.content.endswith(b'</html>'))
